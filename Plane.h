#ifndef PLANE_H
#define PLANE_H

#include "Object.h"

class Plane : public Object
{
    public:
        Plane(Point p=Point(), double d=0, int m=0);
        bool hit(const Ray &r, double &t);
        int getMaterial() const;
        Vector3d getNormale(Point p) const;

    private:
        Point  _position;
        double _d;
        int    _material;
        std::ostream& _edit(std::ostream &os) const;

    friend std::istream& operator >> (std::istream &is, Plane &sph);
};

#include "Plane.hpp"

#endif /*PLANE_H*/
