#ifndef RAY_H
#define RAY_H

#include <iostream>
#include "Vector3d.h"
#include "Point.h"

class Ray
{
    public:
        Ray(Point s=Point(), Vector3d d=Vector3d());
        Point    getPosition()  const;
        Vector3d getDirection() const;

    private:
        Point    _position;
        Vector3d _direction;

    friend std::ostream& operator << (std::ostream &os, const Ray &r);
};

#include "Ray.hpp"

#endif /*RAY_H*/
