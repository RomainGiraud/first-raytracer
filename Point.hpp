#ifndef POINT_HPP
#define POINT_HPP

inline Point::Point(double x, double y, double z)
    : _x(x), _y(y), _z(z)
{
}

inline double Point::operator * (const Vector3d &v)
{
    return _x * v.getX() + _y * v.getY() + _z * v.getZ();
}

inline double Point::operator * (const Point &p)
{
    return _x * p._x + _y * p._y + _z * p._z;
}

inline double Point::getX() const
{
    return _x;
}

inline double Point::getY() const
{
    return _y;
}

inline double Point::getZ() const
{
    return _z;
}

inline std::ostream& operator << (std::ostream &os, const Point &p)
{
    return os << '(' << p._x << ',' << p._y << ',' << p._z << ')';
}

inline std::istream& operator >> (std::istream &is, Point &p)
{
    return is >> p._x >> p._y >> p._z;
}

inline Point Point::operator + (const Vector3d &v)
{
    return Point(_x + v.getX(), _y + v.getY(), _z + v.getZ());
}

inline Point Point::operator - (const Vector3d &v)
{
    return Point(_x - v.getX(), _y - v.getY(), _z - v.getZ());
}

inline Vector3d Point::operator - (const Point &p)
{
    return Vector3d(_x - p._x, _y - p._y, _z - p._z);
}

#endif /*POINT_HPP*/
