#ifndef COLOR_H
#define COLOR_H

#include <iostream>

class Color
{
    public:
        Color(double r=0, double g=0, double b=0);
        double getRed()   const;
        double getGreen() const;
        double getBlue()  const;

    private:
        double _red, _green, _blue;

    friend std::istream& operator >> (std::istream &is, Color &col);
    friend std::ostream& operator << (std::ostream &os, const Color &col);
};

#include "Color.hpp"

#endif /*COLOR_H*/
