#ifndef OBJECT_H
#define OBJECT_H

#include "Point.h"

class Ray;
class Object
{
    public:
        Object() {}
        virtual ~Object() {}
        virtual bool hit(const Ray &r, double &t) = 0;
        virtual int getMaterial() const = 0;
        virtual Point getPosition() const { return _position; }
        virtual Vector3d getNormale(Point p) const = 0;

    protected:
        Point _position;

    private:
        virtual std::ostream& _edit(std::ostream &os) const { return os << "Object"; }

    friend std::ostream& operator << (std::ostream &os, const Object &s)
    {
        return s._edit(os);
    }
};

#endif /*OBJECT_H*/
