#ifndef MATERIEL_H
#define MATERIEL_H

#include <iostream>
#include "Color.h"

class Material
{
    public:
        Material(Color c=Color(), double ref=0);
        Color getColor()      const;
        double getReflection() const;

    private:
        Color _color;
        double _reflection;

    friend std::istream& operator >> (std::istream &is, Material &mat);
    friend std::ostream& operator << (std::ostream &os, const Material &m);
};

#include "Material.hpp"

#endif /*MATERIEL_H*/
