#ifndef MATERIAL_H
#define MATERIAL_H

inline Material::Material(Color c, double ref)
    : _color(c), _reflection(ref)
{
}

inline Color Material::getColor() const
{
    return _color;
}

inline double Material::getReflection() const
{
    return _reflection;
}

inline std::istream& operator >> (std::istream &is, Material &mat)
{
    return is >> mat._color >> mat._reflection; 
}

inline std::ostream& operator << (std::ostream &os, const Material &m)
{
    return os << "color: " << m._color
              << ", reflection: " << m._reflection;
}

#endif /*MATERIAL_H*/
