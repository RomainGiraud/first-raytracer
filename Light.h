#ifndef LUMIERE_H
#define LUMIERE_H

#include <iostream>
#include "Point.h"
#include "Color.h"

class Light
{
    public:
        Light(Point p=Point(), Color c=Color());
        Point getPosition() const;
        Color getColor()    const;

    private:
        Point _position;
        Color _color;

    friend std::istream& operator >> (std::istream &is, Light &lig);
    friend std::ostream& operator << (std::ostream &os, const Light &l);
};

#include "Light.hpp"

#endif /*LUMIERE_H*/
