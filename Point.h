#ifndef POINT_H
#define POINT_H

#include <iostream>
#include "Vector3d.h"

class Point
{
    public:
        Point(double x=0, double y=0, double z=0);
        double getX() const;
        double getY() const;
        double getZ() const;
        Point operator + (const Vector3d &v);
        Point operator - (const Vector3d &v);
        Vector3d operator - (const Point &p);
        double operator * (const Point &p);
        double operator * (const Vector3d &v);

    private:
        double _x, _y, _z;

    friend std::istream& operator >> (std::istream &is, Point &p);
    friend std::ostream& operator << (std::ostream &os, const Point &p);
};

#include "Point.hpp"

#endif /*POINT_H*/
