#include "Scene.h"
#include <fstream>

#include "Ray.h"
#include "Sphere.h"
#include "Plane.h"

using namespace std;

void Scene::draw(string file)
{
    ofstream is (file.c_str(),ios_base::binary);
    if (!is)
        throw string("impossible de charger le fichier de sortie");

    // Ajout du header TGA
    is.put(0).put(0);
    is.put(2);        /* RGB non compresse */

    is.put(0).put(0);
    is.put(0).put(0);
    is.put(0);

    is.put(0).put(0); /* origine X */
    is.put(0).put(0); /* origine Y */

    is.put((_width & 0x00FF))
      .put((_width & 0xFF00) / 256);
    is.put((_height & 0x00FF))
      .put((_height & 0xFF00) / 256);
    is.put(24);       /* 24 bit bitmap */
    is.put(0);
    // fin du header TGA

    // balayage
    for (int y (0); y < _height; ++y)
    {
        for (int x (0); x < _width; ++x)
        {
            double red (0), green (0), blue (0);
            Ray viewRay (Point(x,y,-10000.0),Vector3d(0.0,0.0,1.0));

            double t (20000.0f);
            int currentObject (-1);
            for (unsigned int i (0); i < _objects.size(); ++i)
                if (_objects[i]->hit(viewRay, t))
                    currentObject = i;

            if (currentObject != -1)
            {
                Material currentMat = _materials[_objects[currentObject]->getMaterial()];

                // point de collision sur l'objet
                Point collision = viewRay.getPosition() + viewRay.getDirection()*t;

                // vecteur normal à l'objet (ne fonctionne que pour la sphère)
                Vector3d normale = _objects[currentObject]->getNormale(collision);

                for (unsigned int i (0); i < _lights.size(); ++i)
                {
                    // direction entre le point de collision et la source de lumière
                    Vector3d lightDir = _lights[i].getPosition() - collision;
                    lightDir.normalize();

                    // rayon lumineux qui part de la spère et va à la source de lumière
                    Ray lightRay (collision,lightDir);

                    // test pour savoir si l'objet n'est pas caché par un autre objet
                    t = 20000;
                    bool shadow = false;
                    for (unsigned int j (0); j < _objects.size(); ++j)
                        if (_objects[j]->hit(lightRay, t))
                        {
                            shadow = true;
                            break;
                        }

                    // valeur  de la lumière ambiante
                    double dot = 0.2;
                    if (!shadow)
                    {
                        // produit scalaire
                        // (taux de reception de lumière d'un plan)
                        dot = normale * lightDir;

                        blue  += dot * _lights[i].getColor().getBlue()
                                     * currentMat.getColor().getBlue();
                        green += dot * _lights[i].getColor().getGreen()
                                     * currentMat.getColor().getGreen();
                        red   += dot * _lights[i].getColor().getRed()
                                     * currentMat.getColor().getRed();
                    }
                }
            }

            is.put((unsigned char)min(255.,blue *255))
              .put((unsigned char)min(255.,green*255))
              .put((unsigned char)min(255.,red  *255));
        }
    }
}

void Scene::load(string file)
{
    ifstream is (file.c_str());
    if (!is)
        throw string("impossible de charger le fichier d'entrée");

    is >> _width >> _height;

    int nbMaterials, nbSpheres, nbPlans, nbLights;
    is >> nbMaterials >> nbSpheres >> nbPlans >> nbLights;

    for (int i (0); i < nbMaterials; i++)
    {
        Material mat;
        is >> mat;
        addMaterial(mat);
    }
    for (int i (0); i < nbSpheres; i++)
    {
        Sphere *sph = new Sphere;
        is >> *sph;
        addObject(sph);
    }
    for (int i (0); i < nbPlans; i++)
    {
        Plane *sph = new Plane;
        is >> *sph;
        addObject(sph);
    }
    for (int i (0); i < nbLights; i++)
    {
        Light light;
        is >> light;
        addLight(light);
    }
}
