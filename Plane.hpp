#ifndef PLANE_HPP
#define PLANE_HPP

inline Plane::Plane(Point p, double s, int m)
    : _position(p), _d(s), _material(m)
{
}

inline int Plane::getMaterial() const
{
    return _material;
}

inline Vector3d Plane::getNormale(Point p) const
{
    Vector3d v (_position.getX(),_position.getY(),_position.getZ());
    v.normalize();
    return v;
}

inline bool Plane::hit(const Ray &r, double &t)
{
    Vector3d v (_position.getX(),_position.getY(),_position.getZ());
    v.normalize();
    double A = r.getPosition().getX() * v.getX() +
               r.getPosition().getY() * v.getY() +
               r.getPosition().getZ() * v.getZ() +
               _d;
    double B = r.getDirection().getX() * v.getX() +
               r.getDirection().getY() * v.getY() +
               r.getDirection().getZ() * v.getZ();
    if (!B) return false;
    double t1 = -A/B;
    if (t1 < t && t1 > 0.1)
    {
        t = t1;
        return true;
    }
    return false;
}

inline std::istream& operator >> (std::istream &is, Plane &sph)
{
    return is >> sph._position >> sph._d >> sph._material;
}

inline std::ostream& Plane::_edit(std::ostream &os) const
{
    return os << "[plan] position: " << _position
              << ", D: " << _d
              << ", material: " << _material;
}

#endif /*PLANE_HPP*/
