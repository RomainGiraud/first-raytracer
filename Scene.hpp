#ifndef SCENE_HPP
#define SCENE_HPP

inline Scene::Scene(int widht, int height)
    : _width(widht), _height(height)
{
}

inline Scene::~Scene()
{
    for (unsigned int i (0); i < _objects.size(); ++i)
        delete _objects[i];
}

inline int Scene::getWidth() const
{
    return _width;
}

inline int Scene::getHeight() const
{
    return _height;
}

inline int Scene::getNbMaterials() const
{
    return _materials.size();
}

inline int Scene::getNbObjects() const
{
    return _objects.size();
}

inline int Scene::getNbLights() const
{
    return _lights.size();
}

inline void Scene::addMaterial(const Material &mat)
{
    _materials.push_back(mat);
}

inline void Scene::addObject(Object *obj)
{
    _objects.push_back(obj);
}

inline void Scene::addLight(const Light &lgh)
{
    _lights.push_back(lgh);
}

inline std::ostream& operator << (std::ostream &os, const Scene &s)
{
    os << std::string(80,'-') << std::endl
       << "Scene:"
       << "\twidth: " << s._width
       << ", height: " << s._height << std::endl
       << "\tnb de matériel(s): " << s._materials.size() << std::endl;
    for (unsigned int i (0); i < s._materials.size(); ++i)
        os << "\t\t- " << s._materials[i] << std::endl;
    os << "\tnb d'objet(s): " << s._objects.size() << std::endl;
    for (unsigned int i (0); i < s._objects.size(); ++i)
        os << "\t\t- " << *s._objects[i] << std::endl;
    os << "\tnb de lumière(s): " << s._lights.size() << std::endl;
    for (unsigned int i (0); i < s._lights.size(); ++i)
        os << "\t\t- " << s._lights[i] << std::endl;
    return os << std::string(80,'-') << std::flush;
}

#endif /*SCENE_HPP*/
