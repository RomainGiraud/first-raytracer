#ifndef SPHERE_H
#define SPHERE_H

#include <iostream>
#include "Point.h"
#include "Ray.h"
#include "Object.h"

class Sphere : public Object
{
    public:
        Sphere(Point p=Point(), double s=0, int m=0);
        bool hit(const Ray &r, double &t);
        int getMaterial() const;
        Vector3d getNormale(Point p) const;

    private:
        double _size;
        int    _material;
        std::ostream& _edit(std::ostream &os) const;

    friend std::istream& operator >> (std::istream &is, Sphere &sph);
};

#include "Sphere.hpp"

#endif /*SPHERE_H*/
