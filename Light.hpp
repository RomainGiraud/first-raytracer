#ifndef LIGHT_HPP
#define LIGHT_HPP

inline Light::Light(Point p, Color c)
    : _position(p), _color(c)
{
}

inline Point Light::getPosition() const
{
    return _position;
}

inline Color Light::getColor() const
{
    return _color;
}

inline std::istream& operator >> (std::istream &is, Light &lig)
{
    return is >> lig._position >> lig._color;
}

inline std::ostream& operator << (std::ostream &os, const Light &l)
{
    return os << "position: " << l._position
              << ", color: " << l._color;
}

#endif /*LIGHT_HPP*/
