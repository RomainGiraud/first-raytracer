#ifndef VECTOR3D_HPP
#define VECTOR3D_HPP

#include <cmath>

inline Vector3d::Vector3d(double x, double y, double z)
    : _x(x), _y(y), _z(z)
{
}

inline void Vector3d::normalize()
{
    double n = norme();
    if (n)
    {
        _x /= n;
        _y /= n;
        _z /= n;
    }
}

inline double Vector3d::norme() const
{
    return ::sqrtf((*this)*(*this));
}

inline std::istream& operator >> (std::istream &is, Vector3d &v)
{
    return is >> v._x >> v._y >> v._z;
}

inline std::ostream& operator << (std::ostream &os, const Vector3d &v)
{
    return os << '(' << v._x << ',' << v._y << ',' << v._z << ')';
}

inline Vector3d Vector3d::operator * (double c) const
{
    return Vector3d(_x *c, _y * c, _z * c);
}

inline Vector3d Vector3d::operator - (const Vector3d &v) const
{
    return Vector3d(_x - v._x, _y - v._y, _z - v._z);
}

inline Vector3d Vector3d::operator + (const Vector3d &v) const
{
    return Vector3d(_x + v._x, _y + v._y, _z + v._z);
}

inline double Vector3d::operator * (const Vector3d &v) const
{
    return _x * v._x + _y * v._y + _z * v._z;
}

inline double Vector3d::getX() const
{
    return _x;
}

inline double Vector3d::getY() const
{
    return _y;
}

inline double Vector3d::getZ() const
{
    return _z;
}

#endif /*VECTOR3D_HPP*/
