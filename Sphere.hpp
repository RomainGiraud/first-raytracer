#ifndef SPHERE_HPP
#define SPHERE_HPP

#include <cmath>
#include "Vector3d.h"

inline Sphere::Sphere(Point p, double s, int m)
    : _size(s), _material(m)
{
    _position = p;
}

inline int Sphere::getMaterial() const
{
    return _material;
}

inline Vector3d Sphere::getNormale(Point p) const
{
    Vector3d n = p - _position;
    n.normalize();
    return n;
}

inline bool Sphere::hit(const Ray &r, double &t)
{
    Vector3d v = r.getPosition() - _position;
    double B = r.getDirection() * v;
    double C = v * v - _size * _size;
    double D = B * B - C;
    if (D < 0) return false;

    double t0 = -B - sqrtf(D);
    double t1 = -B + sqrtf(D);
    if ((t0 > 0.1) && (t0 < t))
    {
        t = t0;
        return true;
    }
    if ((t1 > 0.1) && (t1 < t))
    {
        t = t1;
        return true;
    }
    return false;
}

inline std::istream& operator >> (std::istream &is, Sphere &sph)
{
    return is >> sph._position >> sph._size >> sph._material;
}

inline std::ostream& Sphere::_edit(std::ostream &os) const
{
    return os << "[sphère] position: " << _position
              << ", size: " << _size
              << ", material: " << _material;
}

#endif /*SPHERE_HPP*/
