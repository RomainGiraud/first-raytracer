#ifndef SCENE_H
#define SCENE_H

#include <iostream>
#include <vector>
#include <string>
#include "Material.h"
#include "Object.h"
#include "Light.h"

class Scene
{
    public:
        Scene(int width=0, int height=0);
        ~Scene();
        void load(std::string file);
        void draw(std::string file);

        int getWidth()  const;
        int getHeight() const;
        int getNbMaterials() const;
        int getNbObjects()   const;
        int getNbLights()    const;
        void addMaterial(const Material &mat);
        void addObject(Object *obj);
        void addLight(const Light &lgh);

    private:
        std::vector <Material> _materials;
        std::vector <Object*>  _objects;
        std::vector <Light>    _lights;
        int _width, _height;

    friend std::ostream& operator << (std::ostream &os, const Scene &s);
};

#include "Scene.hpp"

#endif /*SCENE_H*/
