#include <iostream>
#include <string>

#include "Scene.h"

using namespace std;

int main(int argc, char *argv[])
{
    try
    {
        if (argc < 3)
            throw string(string("Usage: ")+argv[0]+" input ouput");

        Scene scene;
        scene.load(argv[1]);
        cout << scene << endl;
        scene.draw(argv[2]);
    }
    catch(const string &e)
    {
        cerr << "[erreur] " << e << endl;
    }
    return 0;
}
