#ifndef VECTOR3D_H
#define VECTOR3D_H

#include <iostream>

class Vector3d
{
    public:
        Vector3d(double x=0, double y=0, double z=0);
        double getX() const;
        double getY() const;
        double getZ() const;
        double norme() const;
        void normalize();

        double   operator * (const Vector3d &v) const;
        Vector3d operator * (double c) const;
        Vector3d operator - (const Vector3d &v) const;
        Vector3d operator + (const Vector3d &v) const;

    private:
        double _x, _y, _z;

    friend std::istream& operator >> (std::istream &is, Vector3d &v);
    friend std::ostream& operator << (std::ostream &os, const Vector3d &v);
};

#include "Vector3d.hpp"

#endif /*VECTOR3D_H*/
