#ifndef RAY_HPP
#define RAY_HPP

inline Ray::Ray(Point s, Vector3d d)
    : _position(s), _direction(d)
{
}

inline Point Ray::getPosition() const
{
    return _position;
}

inline Vector3d Ray::getDirection() const
{
    return _direction;
}

inline std::ostream& operator << (std::ostream &os, const Ray &r)
{
    return os << "start: " << r._position
              << ", vecteur: " << r._direction;
}

#endif /*RAY_HPP*/
