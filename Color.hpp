#ifndef COLOR_HPP
#define COLOR_HPP

inline Color::Color(double r, double g, double b)
    : _red(r), _green(g), _blue(b)
{
}

inline double Color::getRed() const
{
    return _red;
}

inline double Color::getGreen() const
{
    return _green;
}

inline double Color::getBlue() const
{
    return _blue;
}

inline std::istream& operator >> (std::istream &is, Color &col)
{
    return is >> col._red >> col._green >> col._blue;
}

inline std::ostream& operator << (std::ostream &os, const Color &col)
{
    return os << '(' << col._red << ", "
                     << col._green << ", "
                     << col._blue << ')' << std::flush;
}

#endif /*COLOR_HPP*/
